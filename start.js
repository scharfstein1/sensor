'use strict';

const net = require('net');
require('dotenv').config();

const Temperatura = require('./clases/Temperatura');
const Oxigeno = require('./clases/Oxigeno');

/**
 * Sensor de temperatura
 */

net.createServer(
    connection => {
        connection
        .on('data', data => {
            let temp = new Temperatura().valor.toString();
            connection.write(temp);
            console.log(`Temperatura: ${temp}`);
        })
        .on('error', error => {
            console.log(error);
        });
    }
)
.listen(process.env.TEMPERATURA_PORT, process.env.TEMPERATURA_HOST, () => {
    console.log(`Sensor de temperatura en puerto: ${process.env.TEMPERATURA_HOST}:${process.env.TEMPERATURA_PORT}`)
})



/**
 * Sensor de Oxigeno
 */

net.createServer(
    connection => {
        connection
        .on('data', data => {
            let oxigeno = new Oxigeno().valor.toString();
            connection.write(oxigeno);
            console.log(`Oxigeno: ${oxigeno}`);
        })
        .on('error', error => {
            console.log(error);
        });
    }
)
.listen(process.env.OXIGENO_PORT, process.env.OXIGENO_HOST, () => {
    console.log(`Sensor de oxigeno en puerto: ${process.env.OXIGENO_HOST}:${process.env.OXIGENO_PORT}`)
});


