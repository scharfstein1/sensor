'use strict';

const Sensor = require('./Sensor');

class Oxigeno extends Sensor{

    constructor(minima = 6, maxima = 15) 
    {
        super(minima, maxima);
    }
}

module.exports = Oxigeno;