'use strict';

const Sensor = require('./Sensor');

class Temperatura extends Sensor{

    constructor(minima = 15, maxima = 25)
    {
        super(minima, maxima);
    }

}

module.exports = Temperatura;