'use strict';

class Sensor {

    constructor(minima, maxima) {
        this.minima = minima;
        this.maxima = maxima;
    }

    get valor() {
        return this.valorAleatorio();
    }

    /**
     * Genera un valor aleatorio segun los valores minimos y maximos establecidos
     * @return double
     */
    valorAleatorio() {
        return Math.random() * (this.maxima - this.minima) + this.minima;;
    }

}

module.exports = Sensor;